//
//  TaskDateFormatter.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/4/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import Foundation

extension Date {
    
    static let componentsDateFormatter = DateComponentsFormatter()
    static let dateFormatter = DateFormatter()
    
    func timeLeft() -> String? {
        if Date() > self {
            return nil
        }
        
        Date.componentsDateFormatter.allowedUnits = [.minute, .hour, .day]
        Date.componentsDateFormatter.unitsStyle = .full
        Date.componentsDateFormatter.zeroFormattingBehavior = .dropAll
        return Date.componentsDateFormatter.string(from: Date(), to: self)
    }
    
    func longDateString() -> String {
        Date.dateFormatter.dateStyle = .long
        Date.dateFormatter.timeStyle = .short
        return Date.dateFormatter.string(from: self)
    }
    
}


