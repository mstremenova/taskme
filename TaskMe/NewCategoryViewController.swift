//
//  NewCategoryViewController.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/5/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class NewCategoryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var categoryNameTextField: UITextField!
    @IBOutlet weak var colorsCollectionView: UICollectionView!
    @IBOutlet weak var saveChangesButton: UIButton!
    @IBOutlet weak var barButtonItem: UIBarButtonItem!
    
    var category:Category?
    
    private var viewModel:CategoryFormViewModel!
    
    // no need for FRC or KVO as colors won't change in any way
    private var colors:[Color] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.colors = Color.fetchAvailableOrdered(inContext: CoreDataStack.shared.context, withCategoryException: self.category)
        self.viewModel = CategoryFormViewModel(withCategory: self.category)
        self.categoryNameTextField.text = self.viewModel.name
        self.saveChangesButton.setTitle(self.viewModel.saveChangesButtonName, for: .normal)
        
        colorsCollectionView.delegate = self
        colorsCollectionView.dataSource = self
        
        
        let frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        self.colorsCollectionView.backgroundView = InfoBackgroundView(withFrame: frame,
                                                                      infoText: Constants.errorOutOfColors,
                                                                      parentView:self.colorsCollectionView)
        self.showPreselectedColor()
    }
    
    
    // MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = self.colors.count
        self.colorsCollectionView.backgroundView?.isHidden = count > 0
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let color = self.colors[indexPath.row]
        
        let cell = self.colorsCollectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.colorCell, for: indexPath)! as ColorCollectionViewCell
        cell.colorView.backgroundColor = color.uiColor
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let color = colors[indexPath.row]
        
        self.tryToPerform(actions:{
            try viewModel.validateAndSet(color: color)
        }, withErrorCompletion:{
            self.colorsCollectionView.deselectItem(at: indexPath, animated: true)
        })
    }
    
    
    // MARK: - Data
    
    private func showPreselectedColor() {
        
        let selectedColorArray = self.colors.filter{
            $0.uiColor == self.viewModel.color
        }
        
        if let selectedColor = selectedColorArray.first,
            let index = self.colors.index(of: selectedColor) {
            self.colorsCollectionView.selectItem(at: IndexPath(row:index,section: 0), animated: true, scrollPosition: .top )
        }
    }
    
    
    @IBAction func textFieldEditingDidChange(_ sender: UITextField) {
        self.tryToPerform(actions: { try self.viewModel.validateAndSet(name:sender.text) })
    }
    
    // MARK: - Navigation
    
    @IBAction func saveChangesButtonTapped(_ sender: Any) {
        
        self.tryToPerform(actions:{
            try viewModel.validate()
            self.viewModel.saveChanges()
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
