//
//  TaskTableViewCell.swift
//  GoTaskYourself
//
//  Created by Tina Stremenova on 3/1/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class TaskTableViewCell: TaskMeTableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var completeButton: CheckBoxButton!
    
}

class TaskMeTableViewCell:UITableViewCell {
    
    override func awakeFromNib() {
        self.contentView.layoutMargins = UIEdgeInsetsMake(Appearence.topMargin, Appearence.sideMargin, Appearence.topMargin, Appearence.sideMargin)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
       
        // wont't call parent class method as it changes color of views in content view
        // super.setSelected(selected, animated: animated)
        
        if selectionStyle == .none {
            return
        }
        
        // this implementation works only for cells with white background
        self.contentView.backgroundColor = selected ? UIColor.lightGray : UIColor.white
    }
}
