//
//  Color+CoreDataProperties.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/2/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import Foundation
import CoreData


extension Color {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Color> {
        return NSFetchRequest<Color>(entityName: "Color");
    }

    @NSManaged public var order: NSNumber
    @NSManaged public var alpha: Int16
    @NSManaged public var blue: Float
    @NSManaged public var green: Float
    @NSManaged public var red: Float
    @NSManaged public var category: Category?

}
