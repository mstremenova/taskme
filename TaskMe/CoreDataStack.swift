//
//  CoreDataStack.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/1/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData

class CoreDataStack: NSObject {
    
    static let shared = CoreDataStack()
    
    private let storeType: String    
    private override init() {
        self.storeType = NSSQLiteStoreType
        super.init()
    }
    
    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1] as NSURL
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional.
        // It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "TaskMe", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator,
        // having added the store for the application to it.
        
        // Create the coordinator and store
        
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("TaskMe.sqlite")
        
        do {
            try coordinator.addPersistentStore(ofType: self.storeType, configurationName: nil, at: url, options: nil)
        } catch {
            do {
                try FileManager.default.removeItem(at: url!)
                try coordinator.addPersistentStore(ofType: self.storeType, configurationName: nil, at: url, options: nil)
            } catch {
                
                NSLog("Failed to initialize the application's saved data")
                // as this app is pretty much useless without database there's no reason to let it run
                abort()
            }
        }
        
        return coordinator
    }()
    
    
    lazy var context: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    func createPrivateChildContext() -> NSManagedObjectContext {
        let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateContext.parent = self.context
        return privateContext
    }
        
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let moc = self.context
        do{
            if moc.hasChanges {
                try moc.save()
            }
        } catch let error as NSError {
            NSLog("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
    }
}

