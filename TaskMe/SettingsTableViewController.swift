//
//  SettingsTableViewController.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/7/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var notificationSwitch: UISwitch!
    private let viewModel:SettingsViewModel = SettingsViewModel()
    
    @IBOutlet weak var sortByDateCell: UITableViewCell!
    @IBOutlet weak var sortByNameCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fillData()
        
        self.tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
    }
    
    
    // MARK: - TableView
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = self.tableView.cellForRow(at: indexPath) else { return }
        guard let reuseIdentifier = cell.reuseIdentifier else { return }
        
        switch reuseIdentifier {
            
        case  R.reuseIdentifier.settingsCategoryCell.identifier: //identifier:
            self.performSegue(withIdentifier: R.segue.settingsTableViewController.showCategoriesFromSettings.identifier, sender: self)
            break
            
        case R.reuseIdentifier.sortByDateCell.identifier,
             R.reuseIdentifier.sortByNameCell.identifier:
            
            let sortType:SortType = (reuseIdentifier == R.reuseIdentifier.sortByDateCell.identifier) ? .ByDueDate : .ByName
            self.viewModel.setSortType(toType: sortType)
            self.updateFilterSelection()
            break
            
        default:
            // no need to do anything
            break
        }
    }
    
    
    // needs to be odne for static cells
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: - Data
    
    private func fillData() {
        
        self.updateFilterSelection()
        self.notificationSwitch.isOn = self.viewModel.areNotificationsOn
    }
    
    private func updateFilterSelection() {
        self.sortByDateCell.accessoryType = self.viewModel.sortType == .ByDueDate ? .checkmark : .none
        self.sortByNameCell.accessoryType = self.viewModel.sortType == .ByName ? .checkmark : .none
        
    }
    
    @IBAction func didSwitchNotificationState(_ sender: UISwitch) {
        self.viewModel.setNotificationState(toState: sender.isOn)
    }
}



