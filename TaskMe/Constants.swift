//
//  Constants.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/10/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class Constants: NSObject {
    
    // Date
    static let overdue = "OVERDUE"
    static let dueIn = "Due in "
    
    // TASK LIST VM 
    static let newSectionHeader = "New"
    static let completedSectionHeader = "Completed"
    
    
    // TASK VM
    static let pickDeadline = "Pick a deadline"
    static let createButton = "Create"
    static let doneButton = "Done"
    static let cancelButton = "Cancel"
    static let saveButton = "Save"
    static let newTaskTitle = "New task"
    static let editTaskTitle = "Edit task"
    
    
    static let errorMissingDate = "Missing due date"
    static let errorOldDate = "You can't set task deadline to the past"
    static let errorMissingTaskName = "Missing task name"
    static let errorNameTooLong = "Task name is too long. Max number of characters is "
    static let errorMissingCategory = "Missing category"
    static let errorMissingColor = "Missing color"
    static let errorColorTaken = "This color is already used in another category"
    static let disabledNotifications = "Notifications are disabled. You can enable them in Settings section"
    static let errorOutOfColors = "It seems you ran out of colors for new categories"
    
    // ERRORS
    
    static let errorTitleSorry = "Sorry"
    static let errorUnknownError = "Unknown error"
    
    // GENERAL
    
    static let confirm = "OK"
}
