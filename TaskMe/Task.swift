//
//  Task+CoreDataClass.swift
//  TaskMe
//
//  Created by Tina Stremenova on 2/28/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import Foundation
import CoreData

@objc(Task)
public class Task: NSManagedObject {
    
    class func saveOrUpdate(withObjectID objectID:NSManagedObjectID?=nil, name:String, category:Category,
                            dueDate:Date, withNotification:Bool, inContext context:NSManagedObjectContext) {
        
        let task:Task = Task.findOrCreate(inContext: context, withObjectID: objectID)
        
        task.name = name
        task.category = category
        task.dueDate = dueDate as NSDate
        task.hasNotification = withNotification
    }
    
    class func changeTaskState(toState completed:Bool, withID objectID: NSManagedObjectID, incontext context:NSManagedObjectContext) {
        guard let task = try? context.existingObject(with: objectID) as! Task else { return }
        task.isCompleted = completed
    }
    
    class func tasksWithNotification(inContext context:NSManagedObjectContext) -> [Task] {
        let fetchRequest:NSFetchRequest<Task> = Task.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "hasNotification == %@", NSNumber(value: true))
        
        guard let fetchResults = try? context.fetch(fetchRequest) else {
            NSLog("Error while fetching tasks with notification")
            return []
        }
        
        return fetchResults
    }
    
    
}


// MARK: - Sort descriptors and predicates
extension Task {
    
    class func taskListSortDescriptor() -> NSSortDescriptor {
        let userSettings = UserSettings.getOrCreateOnlyObject(inContext: CoreDataStack.shared.context)
        
        let sortType:SortType = SortType(rawValue: userSettings.sortType) ?? .ByName
        let sortParameter = sortType == .ByName ? "name" : "dueDate"

        return NSSortDescriptor(key: sortParameter, ascending: true)
    }
    
    class func isCompletedPredicate(isCompleted completed:Bool) -> NSPredicate {
        return NSPredicate(format: "isCompleted == %@", NSNumber(value: completed))
    }
}
