//
//  NotificationScheduler.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/9/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData
import UserNotifications
import UIKit

class NotificationScheduler: NSObject, UNUserNotificationCenterDelegate {
    
    
    // MARK: - Strings
    
    private let objectIdURIKey = "objectIDURI"
    private let notificationTitle = "Overdue task"
    
    private func notificationBody(forTask task:Task) -> String {
        return "Task with name \(task.name) is now overdue"
    }
    
    
    // MARK: - Init
    
    private let notificationCenter = UNUserNotificationCenter.current()
    
    static let shared = NotificationScheduler()
    
    private override init() {
        super.init()
        self.notificationCenter.delegate = self
    }
    
    
    // MARK: - Authorization
    
    func requestAuthorization() {
        self.notificationCenter.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
            if !granted {
                self.deleteAllScheduledNotifications()
            }
        }
    }
    
    class func isAuthorized(completion:@escaping ((Bool) -> Void)) {
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { notificationSettigns in
            completion(notificationSettigns.authorizationStatus == .authorized)
        }
    }
    
    
    
    // MARK: - Displaying notification
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound and show alert to the user
        completionHandler([.alert,.sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
       
        let objectIdUriString = response.notification.request.identifier
        if let objectIdUri = URL(string:objectIdUriString),
            let objectID = CoreDataStack.shared.persistentStoreCoordinator.managedObjectID(forURIRepresentation: objectIdUri) {
            
            if  let task = CoreDataStack.shared.context.object(with: objectID) as? Task,
                let vc = AppDelegate.topViewController(),
                let taskDetail = R.storyboard.main.newTaskTableViewController() {
                
                taskDetail.task = task
                let navEditorViewController: UINavigationController = UINavigationController(rootViewController: taskDetail)
                vc.present(navEditorViewController, animated: true, completion: nil)
            }
        }
        
        completionHandler()
    }
    
    
    
    // MARK: - Scheduler
    
    func scheduleNotification(forTasks tasks:[Task]) {
        for task in tasks {
            self.scheduleNotification(forTask: task)
        }
    }
    
    func rescheduleNotification(forTask task:Task) {
        self.findPendingRequests(forTasks: [task]) { pendingRequests in
            self.notificationCenter.removePendingNotificationRequests(withIdentifiers: pendingRequests.map{ $0.identifier })
            
            self.scheduleNotification(forTask: task)
        }
    }
    
    func deleteScheduledNotification(forTask task:Task) {
        self.findPendingRequests(forTasks: [task]) { foundRequests in
            self.notificationCenter.removePendingNotificationRequests(withIdentifiers: foundRequests.map({$0.identifier}))
        }
    }
    
    func deleteAllScheduledNotifications() {
        notificationCenter.removeAllPendingNotificationRequests()
    }
    
    
    
    // MARK: - Notification Logic
    
    private func findPendingRequests(forTasks tasks:[Task], withCompletion completion:@escaping (([UNNotificationRequest]) -> Void)) {
        
        let taskIDs = tasks.map { $0.objectID.uriRepresentation().absoluteString }
        
        self.notificationCenter.getPendingNotificationRequests { pendingRequests in
            let foundRequests = pendingRequests.filter({
                return taskIDs.contains($0.identifier)
            })
            completion(foundRequests)
        }
    }
    
    private func scheduleNotification(forTask task:Task) {
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = self.notificationTitle
        notificationContent.body = self.notificationBody(forTask: task)
        
        let dateComponents = Calendar.current.dateComponents([.minute, .hour, .day, .month, .year], from: task.dueDate as Date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let request = UNNotificationRequest(identifier: task.objectID.uriRepresentation().absoluteString, content: notificationContent, trigger: trigger)
        self.notificationCenter.add(request) { error in
            guard let error = error else { return }
            NSLog("Error while creating local notification request: \(error)")
        }
    }
    
}
