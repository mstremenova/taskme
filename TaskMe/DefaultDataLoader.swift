//
//  DefaultDataLoader.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/5/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class DefaultDataLoader: NSObject {
    
    static func loadDefaultColors() {
        
        var colorArray:[UIColor] = []
        
        // red 228 37 39
        colorArray.append(UIColor(red: 228/255, green: 37/255, blue: 39/255, alpha: 1))
        
        // orange 246 141 42
        colorArray.append(UIColor(red: 246/255, green: 141/255, blue: 42/255, alpha: 1))
        
        // yellow 255 247 0
        colorArray.append(UIColor(red: 255/255, green: 247/255, blue: 0/255, alpha: 1))
        
        // green 118 191 67
        colorArray.append(UIColor(red: 118/255, green: 191/255, blue: 67/255, alpha: 1))
        
        // turqoise 14 208 194
        colorArray.append(UIColor(red: 14/255, green: 208/255, blue: 194/255, alpha: 1))
        
        // blue 0 179 228
        colorArray.append(UIColor(red: 0/255, green: 179/255, blue: 228/255, alpha: 1))
        
        // purple 113 75 159
        colorArray.append(UIColor(red: 113/255, green: 75/255, blue: 159/255, alpha: 1))
        
        // magenta 210 56 169
        colorArray.append(UIColor(red: 210/255, green: 56/255, blue: 169/255, alpha: 1))
        
        // pink 254 140 210
        colorArray.append(UIColor(red: 254/255, green: 140/255, blue: 210/255, alpha: 1))
        
        // grey 127 117 117
        colorArray.append(UIColor(red: 127/255, green: 117/255, blue: 117/255, alpha: 1))
        
        CotextOperationsHandler.performOnBackground { context in
            
            for color in colorArray {
                Color.create(uiColor: color, inContext: context, inOrder:colorArray.index(of: color)!)
            }
            
            let categoryNames = ["Work", "Fun", "Educations", "Others"]
            let categoryColorIds:[Int] = [0, 3, 1, 9]
            
            for (index, categoryName) in categoryNames.enumerated() {
                
                let colorNumber = NSNumber(integerLiteral: categoryColorIds[index])
                if let color = Color.find(inContext: context, withUniqueIdentifierName: "order", andValue: colorNumber) as? Color {
                    Category.saveOrUpdate(withObjectID: nil, name: categoryName, color: color, inContext: context)
                }
            }
            
            guard let category = Category.find(inContext: context, withUniqueIdentifierName: "name", andValue: "Fun" as NSObject) as? Category else { return }
            
            let taskNames = ["Tap the circle to complete me", "Swipe me to delete", "Tap me to edit"]
            for taskName in taskNames {
                Task.saveOrUpdate(name: taskName, category: category, dueDate: Date(), withNotification: false, inContext: context)
            }
            
            
        }
        
    }
}
