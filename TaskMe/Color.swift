//
//  Color+CoreDataClass.swift
//  TaskMe
//
//  Created by Tina Stremenova on 2/28/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import Foundation
import CoreData
import UIKit

@objc(Color)
public class Color: NSManagedObject {
    
    class func create(uiColor:UIColor, inContext context:NSManagedObjectContext, inOrder order:Int) {
        
        guard let rgbComponnets = uiColor.cgColor.components else {
            NSLog("Could not extract components from UIColor %@", uiColor)
            return
        }
        
        let color:Color = Color.create(inContext: context)
        color.red = Float(rgbComponnets[0])
        color.green = Float(rgbComponnets[1])
        color.blue = Float(rgbComponnets[2])
        color.alpha = Int16(rgbComponnets[3])
        color.order = NSNumber(integerLiteral: order)
    }
    
    class func fetchAvailableOrdered(inContext context:NSManagedObjectContext, withCategoryException category:Category?) -> [Color] {
        
        let predicate = Color.availablePredicate(withCategoryException: category)
        return context.fetch(forClassNamed: Color.className(),predicate: predicate,sortDescriptors: [Color.orderSortDescriptor])
    }
    
    class func fetchAll(inContext context:NSManagedObjectContext) -> [Color] {
        return context.fetch(forClassNamed: Color.className())
    }
}

// MARK: - Convenience functions
extension Color {
    var uiColor:UIColor {
        return UIColor(red: CGFloat(self.red), green: CGFloat(self.green),
                       blue: CGFloat(self.blue), alpha: CGFloat(self.alpha))
    }
    
}


// MARK: - Sort descriptors and predicates
extension Color {
    
    static let orderSortDescriptor = NSSortDescriptor(key: "order", ascending: true)
    
    static func availablePredicate(withCategoryException category:Category?) -> NSPredicate {
        let availablePredicate = NSPredicate(format: "category == nil")
        return category == nil ? availablePredicate :
            NSCompoundPredicate.init(orPredicateWithSubpredicates: [availablePredicate,  NSPredicate(format: "SELF = %@", category!.color.objectID)])
        
    }
    
    
}
