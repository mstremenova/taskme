//
//  CategoryListViewModel.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/20/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData

class CategoryListViewModel: NSObject, NSFetchedResultsControllerDelegate {
    internal var delegate:ViewModelDataManagerDelegate?
    internal var categoryFetchedResultsController:NSFetchedResultsController<NSFetchRequestResult>!
    
    required init(withDelegate delegate:ViewModelDataManagerDelegate) {
        super.init()
        self.delegate = delegate
        
        self.categoryFetchedResultsController = self.createFetchedResultsController()
    }

    func numberOfObjects(inSection section:Int) -> Int {
       return self.categoryFetchedResultsController.fetchedObjects?.count ?? 0
    }
    
    
    func object(atIndexPath indexPath:IndexPath) -> CategoryViewModel {
        let category = self.categoryFetchedResultsController.object(at: indexPath) as! Category
        return CategoryViewModel(withCategory: category)
    }
    

    // MARK: - FRC
    private func createFetchedResultsController() -> NSFetchedResultsController<NSFetchRequestResult> {
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        return NSFetchedResultsControllerCreateMethods.createInMainContext(forEntity: Category.self,
                                                                           withSortDescriptors: [sortDescriptor],
                                                                           withDelegate: self)
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.dataWillChange()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.dataDidChange()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        delegate?.didChange(anObject: anObject, at: indexPath, for: type, newIndexPath: newIndexPath)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        delegate?.didChange(sectionWithIndex: sectionIndex, for: type)
    }

}
