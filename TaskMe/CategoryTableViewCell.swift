//
//  CategoryTableViewCell.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/5/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class CategoryTableViewCell: TaskMeTableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var colorView: UIView!
 
    var showCheckMark:Bool = true
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        self.accessoryType = highlighted && showCheckMark ? .checkmark : .none
    }
}
