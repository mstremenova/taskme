//
//  Settings+CoreDataClass.swift
//  TaskMe
//
//  Created by Tina Stremenova on 2/28/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import Foundation
import CoreData


enum SortType:Int16 {
    case ByDueDate
    case ByName
}

@objc(UserSettings)
public class UserSettings: NSManagedObject {
    
    class func saveOrUpdate(withSortType sortType:SortType, withNotifications notificationsOn:Bool, inContext context:NSManagedObjectContext) {
        
        let settings:UserSettings = UserSettings.getOrCreateOnlyObject(inContext: context)
        settings.areNotificationsOn = notificationsOn
        settings.sortType = sortType.rawValue
    }
    
    class func getOrCreateOnlyObject(inContext context:NSManagedObjectContext) -> UserSettings {
        guard let fetchedSettings:[UserSettings] = try? context.fetch(UserSettings.fetchRequest()) else {
            NSLog("Couldn't fetch settings")
            abort()
        }
        
        switch fetchedSettings.count {
        case 0:
            return UserSettings.create(inContext: context) as UserSettings
        case 1:
            return fetchedSettings.first! as UserSettings
        default:
            // we don't want this to happen
            NSLog("Too many results for Settings: %@", fetchedSettings.count)
            abort()
        }
    }
    
}
