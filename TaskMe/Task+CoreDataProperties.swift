//
//  Task+CoreDataProperties.swift
//  TaskMe
//
//  Created by Tina Stremenova on 2/28/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import Foundation
import CoreData


extension Task {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Task> {
        return NSFetchRequest<Task>(entityName: "Task");
    }

    @NSManaged public var name: String
    @NSManaged public var dueDate: NSDate
    @NSManaged public var hasNotification: Bool
    @NSManaged public var isCompleted: Bool
    @NSManaged public var category: Category

}
