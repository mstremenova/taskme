//
//  InfoBackgroundView.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/13/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class InfoBackgroundView: UIView {
    
    var infoLabel:UILabel!
    
    init(withFrame frame:CGRect, infoText:String, parentView:UIView?=nil) {
        super.init(frame: frame)
        
        self.setUp()
        
        self.infoLabel.text = infoText
        
        // not necesarry when view is used as backgroundView
        guard let parentView = parentView else { return }
        parentView.addSubview(self)
        self.center = parentView.center        
    }
    
   private override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    internal required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setUp() {
        
        self.infoLabel = UILabel(frame: self.frame)
        self.addSubview(self.infoLabel)
        self.infoLabel.numberOfLines = 0
        self.infoLabel.lineBreakMode = .byWordWrapping
        self.infoLabel.textAlignment = .center
        self.infoLabel.textColor = UIColor.darkGray
        self.infoLabel.font = UIFont.taskMeInfoFont
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.infoLabel.center = self.center

    }
}
