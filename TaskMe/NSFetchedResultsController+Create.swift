//
//  NSFetchedResultsController+Create.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/1/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData


// theres problem with generics functions in generic types extensions, therefore needed to use struct or simply custom class
struct NSFetchedResultsControllerCreateMethods {
    
    static func create<T:NSManagedObject>(forEntity entity: T.Type, inContext context: NSManagedObjectContext,
                       withSortDescriptors sortDescriptors:[NSSortDescriptor],
                       withPredicate predicate:NSPredicate?=nil,
                       withDelegate delegate:NSFetchedResultsControllerDelegate,
                       withSectionNameKeyPath sectionNameKeyPath:String?=nil) -> NSFetchedResultsController<NSFetchRequestResult> {
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.className())
        
        if predicate != nil {
            fetchRequest.predicate = predicate
        }
        
        fetchRequest.sortDescriptors = sortDescriptors
        
        let controller:NSFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: sectionNameKeyPath, cacheName: nil)
        controller.delegate = delegate
        
        // perform initial model fetch
        do {
            try controller.performFetch()
        } catch let error as NSError {
            print("fetch error: \(error.localizedDescription)")
            abort();
        }
        
        return controller
    }
    
    static func createInMainContext<T:NSManagedObject>(forEntity entity: T.Type,
                                    withSortDescriptors sortDescriptors:[NSSortDescriptor],
                                    withPredicate predicate:NSPredicate?=nil,
                                    withDelegate delegate:NSFetchedResultsControllerDelegate,
                                    withSectionNameKeyPath sectionNameKeyPath:String?=nil) -> NSFetchedResultsController<NSFetchRequestResult> {
        let mainContext = CoreDataStack.shared.context
        return NSFetchedResultsControllerCreateMethods.create(forEntity: entity, inContext: mainContext, withSortDescriptors: sortDescriptors, withPredicate: predicate, withDelegate:delegate, withSectionNameKeyPath: sectionNameKeyPath)
    }

}
