//
//  Errors.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/11/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

enum ValidationError: Error {
    case missingName
    case nameTooLong(maxNumber:Int)
}

enum TaskValidationError: Error {
    case missingCategory
    case missingDueDate
    case dueDateTooOld
    case notificationsDisabled
}

enum CategoryValidationError: Error {
    case colorTaken
    case missingColor
}

struct ErrorHandler {
    static func handle(error:Error, inView viewController:UIViewController) {
        
        let title = Constants.errorTitleSorry
        var message = ""
        
        switch error {
        case ValidationError.missingName:
            message.append(Constants.errorMissingTaskName)
            break
        case ValidationError.nameTooLong(let maxValue):
            message.append(Constants.errorNameTooLong + String(maxValue))
            break
        case TaskValidationError.missingDueDate:
            message.append(Constants.errorMissingDate)
            break
        case TaskValidationError.dueDateTooOld:
            message.append(Constants.errorOldDate)
            break
        case TaskValidationError.missingCategory:
            message.append(Constants.errorMissingCategory)
            break
        case TaskValidationError.notificationsDisabled:
            message.append(Constants.disabledNotifications)
            break
        case CategoryValidationError.colorTaken:
            message.append(Constants.errorColorTaken)
        case CategoryValidationError.missingColor:
            message.append(Constants.errorMissingColor)
        default:
            message.append(Constants.errorUnknownError)
        }
        
        viewController.showInfoMessage(withTitle: title, andMessage: message)
    }
    
    
}


