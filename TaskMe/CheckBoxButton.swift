//
//  CheckButton.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/13/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class CheckBoxButton: UIButton {
    
    var isChecked:Bool = false {
        didSet {
            let buttonImage = isChecked ? UIImage(named: R.image.checked.name) : UIImage(named: R.image.unchecked.name)
            self.setImage(buttonImage, for: .normal)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setUp()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setUp()
    }
    
    private func setUp() {
        self.addTarget(self, action: #selector(self.tapped), for: .touchUpInside)
    }
    
    @objc private func tapped() {
        self.isChecked = !isChecked
    }
}
