//
//  SettingsCategoryTableViewController.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/7/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class SettingsCategoryTableViewController: CategoryListTableViewController {
    
    @IBOutlet weak var newCategoryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newCategoryButton.isHidden = false
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = super.tableView(tableView, cellForRowAt: indexPath) as! CategoryTableViewCell
        cell.showCheckMark = false
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categoryViewModel = self.viewModel.object(atIndexPath: indexPath)
        self.showEditForm(forCategory: categoryViewModel.category)
    }
    
    private func showEditForm(forCategory category:Category) {
        if let newCategoryViewController = R.storyboard.main.newCategoryViewController() {
            newCategoryViewController.category = category
            self.present(newCategoryViewController, animated: true, completion: nil)
        }
    }
    
}
