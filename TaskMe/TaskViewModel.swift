//
//  TaskViewModel.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/4/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit
import CoreData

class TaskViewModel:NSObject {
    
    var task:Task!
    fileprivate let userSettings = UserSettings.getOrCreateOnlyObject(inContext: CoreDataStack.shared.context)
    
    init(withTask task:Task) {
        self.task = task
        super.init()
    }
    
    // MARK: - Task
    
    var name:String {
        return self.task.name
    }
    
    var hasNotification:Bool {
        if !userSettings.areNotificationsOn { return false }
        return self.task.hasNotification
    }
    
    var isCompleted:Bool {
        return self.task.isCompleted
    }
    
    // MARK: - Category
    
    var categoryName:String {
        return task.category.name
    }
    
    var categoryColor:UIColor {
        return self.task.category.color.uiColor
    }
    
    
    // MARK: - Due Date
    var longDueDateString:String {
        
        return [self.dueDateString(),
                self.dueDateTimeLeft()].joined(separator: "\n")
    }
    
    var timeLeftString:String {
        return self.dueDateTimeLeft()
    }

    var dueDate:Date {
        return self.task.dueDate as Date
    }
    
    func dueDateString() -> String {
        let date = task.dueDate as Date
        return date.longDateString()
    }
    
    private func dueDateTimeLeft() -> String {
        
        guard let timeLeft = (self.task.dueDate as Date).timeLeft() else {
            return self.task.isCompleted ? "" : Constants.overdue
        }
        return Constants.dueIn + timeLeft
    }
}




class TaskFormViewModel:TaskViewModel {
    private let temporaryContext:NSManagedObjectContext!
    private var isNewTask:Bool
    
    
    override init(withTask passedTask:Task?) {
        self.isNewTask = passedTask == nil
        self.temporaryContext = CoreDataStack.shared.createPrivateChildContext()
        super.init(withTask: Task.findOrCreate(inContext: temporaryContext, withObjectID: passedTask?.objectID) as Task)
        
        if isNewTask { self.setDueDate(dueDate: Date()) }
    }
    
    var saveChangesButtonName:String {
        return isNewTask ? Constants.createButton : Constants.saveButton
    }
    
    var title:String {
        return isNewTask ? Constants.newTaskTitle : Constants.editTaskTitle
    }
    
    private func setNotification(newValue:Bool) {
        self.temporaryContext.perform {
            self.task.hasNotification = newValue
        }
    }
    
    private func setName(name:String) {
        self.temporaryContext.perform {
            self.task.name = name
        }
    }
    
    private func setDueDate(dueDate:Date) {
        self.temporaryContext.perform {
            self.task.dueDate = dueDate as NSDate
        }
    }
    
    private func setCategory(category:Category) {
        self.temporaryContext.perform {
            guard let fetchedCategory = try? self.temporaryContext.existingObject(with: category.objectID) else { return }
            self.task.category = fetchedCategory as! Category
        }
    }
    
    
    // public setter as there's no need for validation
    func setState(toCompleted completed:Bool) {
        self.temporaryContext.perform {
            self.task.isCompleted = completed
        }
    }
    
    
    // MARK: - Data validation
    
    // MARK: DueDate Validation
    
    func validateAndSet(date:Date?) throws {
        
        try self.validate(dueDate: date)
        self.setDueDate(dueDate: date!)
    }
    
    private func validate(dueDate:Date?) throws {
        
        guard let date = dueDate else {
            throw TaskValidationError.missingDueDate
        }
        
        if date < Date() {
            throw TaskValidationError.dueDateTooOld
        }
    }
    
    
    // MARK: Name Validation
    
    func validateAndSet(name:String?) throws {
        try self.validate(name: name)
        self.setName(name: name!)
    }
    
    private func validate(name:String?) throws {
        guard let taskName = name else {
            throw ValidationError.missingName
        }
        
        if taskName.characters.count > 25 {
            throw ValidationError.nameTooLong(maxNumber: 25)
        }
    }
    
    
    // MARK: Category Validation
    
    func validateAndSet(category:Category?) throws {
        try self.validate(category: category)
        self.setCategory(category: category!)
    }
    
    private func validate(category:Category?) throws {
        guard let _ = category else {
            throw TaskValidationError.missingCategory
        }
    }
    
    // MARK: Notification Validation
    
    // no need for separate validation as the validation is not part of the global form alidation due to notifications default value
    func validateAndSet(notificationState:Bool) throws {
        if !self.userSettings.areNotificationsOn {
            throw TaskValidationError.notificationsDisabled
        }
        self.setNotification(newValue: notificationState)
    }
    
    
    // MARK: Global Task Form Validation
    
    func validate() throws {
        
        try self.validate(name: self.task.name)
        try self.validate(dueDate: self.task.dueDate as Date)
        try self.validate(category: self.task.category)        
    }
    
    func saveChanges() {
        self.temporaryContext.saveWithParent(withCompletion: {
            self.setNotifications()
        })
    }
    
    // MARK: - Notifications
    
    private func setNotifications() {
        if self.task.hasNotification && !self.task.isCompleted {
            NotificationScheduler.shared.rescheduleNotification(forTask: self.task)
        } else {
            NotificationScheduler.shared.deleteScheduledNotification(forTask: self.task)
        }
    }
    
}

