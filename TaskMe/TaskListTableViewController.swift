//
//  ViewController.swift
//  GoTaskYourself
//
//  Created by Tina Stremenova on 2/28/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit
import CoreData

class TaskListTableViewController: TableViewControllerWithDataSource {
    
    fileprivate var viewModel:TaskListViewModel!
    
    override func viewDidLoad() {
        self.viewModel = TaskListViewModel(withDelegate: self)
        super.viewDidLoad()
    }
    
    // MARK: - User Action
    
    @objc fileprivate func changeTaskStateButtonTapped(sender:UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        guard let indexPath = self.tableView.indexPathForRow(at: buttonPosition) else { return }
        
        let taskViewModel = self.viewModel.object(atIndexPath: indexPath)
        self.viewModel.changeTaskState(atIndexPath: indexPath, toState: !taskViewModel.isCompleted)
    }
}

// MARK: - TableView
extension TaskListTableViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfObjects(inSection: section)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.viewModel.sectionName(inSection: section)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let taskViewModel = self.viewModel.object(atIndexPath: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.taskCell.identifier, for: indexPath) as! TaskTableViewCell
        cell.nameLabel.text = taskViewModel.name
        cell.dueDateLabel.text = taskViewModel.longDueDateString
        
        cell.completeButton.addTarget(self, action: #selector(self.changeTaskStateButtonTapped(sender:)), for: .touchUpInside)
        cell.completeButton.backgroundColor = taskViewModel.categoryColor
        cell.completeButton.isChecked = taskViewModel.isCompleted
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let taskViewModel = self.viewModel.object(atIndexPath: indexPath)
        self.showEditForm(forTakViewmodel: taskViewModel)
    }
    
    private func showEditForm(forTakViewmodel taskViewModel: TaskViewModel) {
        guard let newTaskTableViewController = R.storyboard.main.newTaskTableViewController() else { return }
        newTaskTableViewController.task = taskViewModel.task
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: newTaskTableViewController)
        self.present(navEditorViewController, animated: true, completion: nil)
    }
    
    // MARK: - swipe actions
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        // needs to be implemented too or swipe won't display the actions
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteTask = UITableViewRowAction(style: .normal, title: "Delete") { action, index in
            self.viewModel.deleteTask(atIndexPath: indexPath)
        }
        deleteTask.backgroundColor = UIColor.red
        
        return [deleteTask]
    }
    
}
