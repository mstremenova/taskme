//
//  CategoryViewModel.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/5/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit
import CoreData

class CategoryViewModel: NSObject {
    let category:Category!
    
    init(withCategory category:Category) {
        self.category = category
        super.init()
    }
    
    var name:String? {
        return category.name
    }
    
    var color:UIColor? {
        return category.color.uiColor
    }
    
}

final class CategoryFormViewModel:CategoryViewModel {
    
    private let temporaryContext:NSManagedObjectContext!
    private let isNewTask:Bool
    
    override init(withCategory passedCategory:Category?) {
        isNewTask = passedCategory == nil
        temporaryContext = CoreDataStack.shared.createPrivateChildContext()
        super.init(withCategory: Category.findOrCreate(inContext: temporaryContext, withObjectID: passedCategory?.objectID))
        
    }
    
    private func setName(name:String) {
        temporaryContext.perform {
            self.category.name = name
        }
    }
    
    
    // need to be able to setColor without validation as it will not be very plausible to have invalid color
    private func setColor(color:Color) {
        temporaryContext.perform {
            guard let fetchedColor = try? self.temporaryContext.existingObject(with: color.objectID) else { return }
            self.category.color = fetchedColor as! Color
        }
    }
    
    func validateAndSet(name: String?) throws {
        try self.validate(name: name)
        self.setName(name: name!)
    }
    
    private func validate(name:String?) throws {
        guard let taskName = name else {
            throw ValidationError.missingName
        }
        
        if taskName.characters.count == 0 {
            throw ValidationError.missingName
        }
        
        if taskName.characters.count > 25 {
            throw ValidationError.nameTooLong(maxNumber: 25)
        }
    }

    
    
    func validateAndSet(color: Color?) throws {
        try self.validate(color: color)
        self.setColor(color: color!)
    }

    
    private func validate(color:Color?) throws {
        guard let categoryColor = color else {
            throw CategoryValidationError.missingColor
        }
        
        if categoryColor.category != nil
            && self.category.color.order != categoryColor.order {
            throw CategoryValidationError.colorTaken
        }
    }

    func validate() throws {
        try self.validate(name: self.category.name)
        try self.validate(color: self.category.color)
    }
    
       
    func isColorAvailable(color:Color) -> Bool {
        return color.category == nil
    }
    
    
    var saveChangesButtonName:String {
        return isNewTask ? "Create" : "Save"
    }
    
    func saveChanges() {
        self.temporaryContext.perform {
            self.temporaryContext.saveWithParent()
        }
    }
    
}
