//
//  TableViewController.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/5/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit
import CoreData


protocol DataPickerDelegate {
    func didSelect(_ object:NSManagedObject)
}

class CategoryListTableViewController: TableViewControllerWithDataSource {

    var delegate:DataPickerDelegate?
    var selectedCategory:Category?
    internal var viewModel:CategoryListViewModel!
    
    override func viewDidLoad() {
        
        self.viewModel = CategoryListViewModel(withDelegate: self)
        self.tableView.register(UINib(nibName: R.nib.categoryTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.reuseIdentifier.categoryCell.identifier)
        
        super.viewDidLoad()
    }
    
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfObjects(inSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let viewModel = self.viewModel.object(atIndexPath: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.categoryCell, for: indexPath)! as CategoryTableViewCell
        
        cell.nameLabel.text = viewModel.name
        cell.colorView.backgroundColor = viewModel.color
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewModel = self.viewModel.object(atIndexPath: indexPath)
        self.delegate?.didSelect(viewModel.category)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

