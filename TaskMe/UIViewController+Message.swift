//
//  UIViewController+Message.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/7/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showInfoMessage(withTitle title:String, andMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: Constants.confirm, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Error handling
    
    internal func tryToPerform(actions:(() throws -> Void), withErrorCompletion:(() -> Void)?=nil) {
        do {
            try actions()
        } catch let error {
            ErrorHandler.handle(error: error, inView: self)
            withErrorCompletion?()
        }
    }
    
    
}
