//
//  NSManagedObjectContext + Functions.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/1/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData

extension NSManagedObjectContext {
    
     // MARK: - Child context
    
    func saveWithParent(withCompletion completion:(() -> Void)?=nil) {
        
        do {
            try self.save()
        } catch {
            NSLog("Could not save context: \(error)")
        }
        
        if let parentContext = self.parent {
            parentContext.perform {
                do {
                    try parentContext.save()
                    completion?()
                } catch {
                    NSLog("Could not save context: \(error)")
                }
            }
        }
    }
    
    // MARK: - Data
    
    func fetch<T: NSManagedObject>(forClassNamed className:String,predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor] = []) -> [T] {
        
        let fetchRequest = NSFetchRequest<T>(entityName: className)
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors = sortDescriptors
        do {
            return try self.fetch(fetchRequest) 
        } catch {
            fatalError("Failed to fetch objects: \(error)")
        }
        
        return []
    }
    
    
    
}
