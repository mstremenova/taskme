//
//  CircleView.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/5/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class CircleView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setCorners()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         self.setCorners()
    }
    
    private func setCorners() {
        self.layer.cornerRadius = self.layer.frame.height/2
    }

}
