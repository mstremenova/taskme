//
//  TaskFormViewController.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/4/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit
import CoreData
import DatePickerDialog

class NewTaskTableViewController: UITableViewController, DataPickerDelegate {
    
    @IBOutlet weak var taskNameTextField: UITextField!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryColorView: UIView!
    @IBOutlet weak var pickDueDateButton: UIButton!
    @IBOutlet weak var taskNotificationSwitch: UISwitch!
    @IBOutlet weak var saveChangesButton: UIButton!
    @IBOutlet weak var changeStateButton: CheckBoxButton!
    
    var task:Task?
    private var viewModel:TaskFormViewModel!
    private var categoryViewModel:CategoryViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fillFormWithData()
        
        self.tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        
    }
    
    
    // MARK: - Form Data
    
    private func fillFormWithData() {
        self.viewModel = TaskFormViewModel(withTask: task)
        self.saveChangesButton.setTitle(viewModel.saveChangesButtonName, for: .normal)
        self.navigationItem.title = self.viewModel.title
        
        if task != nil {
            self.taskNameTextField.text = viewModel.name
            self.pickDueDateButton.setTitle(viewModel.longDueDateString, for: .normal)
            self.categoryNameLabel.text = viewModel.categoryName
            self.categoryColorView.backgroundColor = viewModel.categoryColor
            self.taskNotificationSwitch.isOn = viewModel.hasNotification
            self.changeStateButton.isChecked = viewModel.isCompleted
        }
    }
    
    
    func didSelect(_ object: NSManagedObject) {
        
        self.tryToPerform(actions: {
            try viewModel.validateAndSet(category: object as? Category)
            self.categoryViewModel = CategoryViewModel(withCategory: object as! Category)
            self.categoryNameLabel.text = self.categoryViewModel?.name
            self.categoryColorView.backgroundColor = self.categoryViewModel?.color
            
        })
    }
    
    
    @IBAction func pickDueDateButtonTapped(_ sender: Any) {
        
        DatePickerDialog().show(title:Constants.pickDeadline, doneButtonTitle: Constants.doneButton, cancelButtonTitle: Constants.cancelButton, defaultDate: viewModel.dueDate, minimumDate: Date(), maximumDate: nil, datePickerMode: .dateAndTime) { pickedDate in
            
            // user cancelled the choise, so it does not make sense to validate nil input
            if pickedDate == nil {
                return
            }
            
            self.tryToPerform(actions: {
                try self.viewModel.validateAndSet(date: pickedDate)
                self.pickDueDateButton.setTitle(self.viewModel.dueDateString(), for: .normal)
            })
        }
    }
    
    @IBAction func changeStateButtonTapped(_ sender: CheckBoxButton) {
        self.viewModel.setState(toCompleted: sender.isChecked)
    }
    
    // MARK: - TableView
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath)
        if cell?.reuseIdentifier == R.reuseIdentifier.categoriesCell.identifier {
            self.performSegue(withIdentifier: R.segue.newTaskTableViewController.showCategoriesSegue, sender: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    // MARK: - User Action
    
    @IBAction func notificationSwitchTapped(_ sender: UISwitch) {
        
        self.tryToPerform(actions:{
            try self.viewModel.validateAndSet(notificationState: sender.isOn)
        }, withErrorCompletion: {
            sender.isOn = false
        })
        
    }
    
    @IBAction func confirmButtonTapped(_ sender: Any) {
        
        self.tryToPerform(actions:{
            try self.viewModel.validate()
            self.viewModel.saveChanges()
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    
    @IBAction func textFieldEditingDidEnd(_ sender: UITextField) {
        if sender == self.taskNameTextField {
            self.tryToPerform(actions:{
                try self.viewModel.validateAndSet(name: sender.text)
            }, withErrorCompletion: {
                    self.taskNameTextField.becomeFirstResponder()
            })
        }
    }
    
    
    //MARK: - Navigation
    
    @IBAction func dismissButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let categoriesVC = segue.destination as? CategoryListTableViewController {
            categoriesVC.delegate = self
        }
    }
    
}

