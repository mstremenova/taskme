//
//  NSManagedObject+Functions.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/1/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData

extension NSManagedObject {
    
    // uniqueValue is optional specifically for the work with CoreData NSManagedObjectID
    static func findOrCreate<T: NSManagedObject>(inContext context:NSManagedObjectContext, withObjectID objectID:NSManagedObjectID?) -> T {
        
        guard let managedObjectID = objectID,
            let existingObject = try? context.existingObject(with: managedObjectID) else {
                return T.create(inContext: context)
        }
        
        return existingObject as! T
    }
    
    
    
    // MARK: - Generic functions for generic unique value fetch
    
    static func findOrCreate<T: NSManagedObject>(inContext context:NSManagedObjectContext, withUniqueIdentifierName uniqueIdName:String, andValue uniqueIdValue:NSObject) -> T {
        
        guard let managedObject = T.find(inContext: context, withUniqueIdentifierName: uniqueIdName, andValue: uniqueIdValue) else {
            return T.create(inContext: context)
        }
        return managedObject as! T
    }
    
    
    static func create<T: NSManagedObject> (inContext context:NSManagedObjectContext) -> T {
        guard let entityDescription = NSEntityDescription.entity(forEntityName: T.description(), in: context) else {
            NSLog("Could not get entity description for entity \(T.self)")
            abort()
        }
        let instance = NSManagedObject(entity: entityDescription, insertInto: context)
        return instance as! T
    }
    
    
    static func find<T: NSManagedObject>(inContext context:NSManagedObjectContext, withUniqueIdentifierName uniqueIdName:String, andValue uniqueIdValue:NSObject) -> T? {
        
        let predicate = NSPredicate(format: "\(uniqueIdName) == %@", uniqueIdValue)
        let fetchedResults = context.fetch(forClassNamed: self.className(),predicate:predicate, sortDescriptors: [])
        
        if fetchedResults.count > 1 {
            NSLog("Too many fetched results for entity \(T.self) with \(uniqueIdName) \(uniqueIdValue)")
        }
        
        return fetchedResults.first as? T
    }
}

extension NSObject {
    static func className() -> String {
        return String(describing: self)
    }
}
