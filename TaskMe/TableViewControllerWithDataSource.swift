//
//  TableViewControllerWithDataSource.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/17/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit
import CoreData

protocol ViewModelDataManagerDelegate {
    func dataWillChange()
    func dataDidChange()
    func didResetData()
    func didChange(anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
    func didChange(sectionWithIndex sectionIndex: Int, for type: NSFetchedResultsChangeType)
}

class TableViewControllerWithDataSource: UITableViewController, ViewModelDataManagerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    
    func dataWillChange() {
        self.tableView.beginUpdates()
    }
    
    func dataDidChange() {
        self.tableView.endUpdates()
    }
    
    func didResetData() {
        self.tableView.reloadData()
    }
    
    func didChange(sectionWithIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .move, .update:
            break
        }
    }
    
    func didChange(anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            self.tableView.reloadRows(at: [indexPath!], with: .fade)
        case .move:
            tableView.deleteRows(at: [indexPath!], with: .fade)
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        }
    }
    
}
