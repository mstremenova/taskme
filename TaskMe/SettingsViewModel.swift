//
//  SettingsViewModel.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/13/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class SettingsViewModel:NSObject {
    
    private let settings:UserSettings!
    private var previousNotificationValue:Bool
    
    override init() {
        settings = UserSettings.getOrCreateOnlyObject(inContext: CoreDataStack.shared.context)
        previousNotificationValue = settings.areNotificationsOn
        super.init()
    }
    
    var sortType:SortType {
        guard let sortType = SortType(rawValue: self.settings.sortType) else {
            return SortType.ByDueDate
        }
        return sortType
    }
    
    var areNotificationsOn:Bool {
        return self.settings.areNotificationsOn
    }
    
    func setNotificationState(toState state:Bool) {
        self.settings.areNotificationsOn = state
    }
    
    func setSortType(toType type:SortType) {
        self.settings.sortType = type.rawValue
    }
    
    func saveChanges() {
        CoreDataStack.shared.context.saveWithParent(withCompletion: {
            self.setNotifications()
        })
    }
    
    private func setNotifications() {
        if self.previousNotificationValue != self.settings.areNotificationsOn {
            
            if self.settings.areNotificationsOn {
                let taskWithNotifications = Task.tasksWithNotification(inContext: CoreDataStack.shared.context)
                NotificationScheduler.shared.scheduleNotification(forTasks: taskWithNotifications)
            } else {
                NotificationScheduler.shared.deleteAllScheduledNotifications()
            }
            
        }
    }
}
