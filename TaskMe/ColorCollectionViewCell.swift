//
//  ColorCollectionViewCell.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/5/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class ColorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var colorView: CircleView!
    
    override var isSelected: Bool {
       didSet {
            self.colorView.layer.borderColor = isSelected ? UIColor.darkGray.cgColor : UIColor.clear.cgColor
            self.colorView.layer.borderWidth = 2.5
        }
    }
    
    
}
