//
//  Appearence.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/11/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import UIKit

class Appearence {
    
    
    static let sideMargin:CGFloat = 15
    static let topMargin:CGFloat = 10
    
    
    class func setAppearence(forApplication application:UIApplication) {
        if let delegate = application.delegate as? AppDelegate,
            let window = delegate.window {
                window.tintColor = UIColor.taskMeGreenColor
        }
    }
}

extension UIColor {
    static let taskMeGreenColor =  UIColor(red: 0/255, green: 128/255, blue: 0/255, alpha: 1)
}

extension UIFont {
    static let taskMeInfoFont = UIFont (name: "System", size: 14)
}

