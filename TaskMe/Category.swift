//
//  Category+CoreDataClass.swift
//  TaskMe
//
//  Created by Tina Stremenova on 2/28/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import Foundation
import CoreData

@objc(Category)
public class Category: NSManagedObject {
    
    class func saveOrUpdate(withObjectID objectID:NSManagedObjectID?, name:String, color:Color, inContext context:NSManagedObjectContext) {
        
        let category:Category = Category.findOrCreate(inContext: context, withObjectID: objectID)
        
        category.name = name
        category.color = color
    }

}
