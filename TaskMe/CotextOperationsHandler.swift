//
//  CotextOperationsHandler.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/1/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData

class CotextOperationsHandler: NSObject {
    
    
    static func performOnBackground(backgroundActions: @escaping (NSManagedObjectContext) -> Void) {
        
        let privateChildContext = CoreDataStack.shared.createPrivateChildContext()
        
        privateChildContext.perform {
            backgroundActions(privateChildContext)            
            privateChildContext.saveWithParent()
        }
    }
    
    static func performOnMain(action: @escaping (NSManagedObjectContext) -> Void) {
        let mainContext = CoreDataStack.shared.context
        
        mainContext.perform {
            action(mainContext)
            
             CoreDataStack.shared.saveContext()
        }
    }
    
}

