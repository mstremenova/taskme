//
//  TaskListViewModel.swift
//  TaskMe
//
//  Created by Tina Stremenova on 3/10/17.
//  Copyright © 2017 Tina Stremenova. All rights reserved.
//

import CoreData


class TaskListViewModel: NSObject, NSFetchedResultsControllerDelegate {
    
    var delegate:ViewModelDataManagerDelegate?
    
    private var newTasksFRC:NSFetchedResultsController<NSFetchRequestResult>!
    private var completedTasksFRC:NSFetchedResultsController<NSFetchRequestResult>!
    @objc private let userSettings = UserSettings.getOrCreateOnlyObject(inContext: CoreDataStack.shared.context)
    
    required init(withDelegate delegate:ViewModelDataManagerDelegate) {
        super.init()
        self.delegate = delegate
        
        self.newTasksFRC = self.createTasksFetchedResultsController(completed: false)
        self.completedTasksFRC = self.createTasksFetchedResultsController(completed: true)
        
        addObserver(self, forKeyPath: #keyPath(userSettings.sortType), options: [.new], context: nil)
    }
    
    func numberOfSections() -> Int {
        return 2
    }
    
    func numberOfObjects(inSection section:Int) -> Int {
        if section == 0 {
            return self.newTasksFRC.fetchedObjects?.count ?? 0
        }
        return self.completedTasksFRC.fetchedObjects?.count ?? 0
    }
    
    func sectionName(inSection section:Int) -> String {
        return section == 0 ? Constants.newSectionHeader : Constants.completedSectionHeader
    }
    
    
    func object(atIndexPath indexPath:IndexPath) -> TaskViewModel {
        let task = self.objectFrom(publicIndexPath: indexPath)
        return TaskViewModel(withTask: task)
    }
    
    
    func changeTaskState(atIndexPath indexPath:IndexPath, toState completed:Bool) {
        let task = self.objectFrom(publicIndexPath: indexPath)
        CotextOperationsHandler.performOnBackground(backgroundActions: { context in
            Task.changeTaskState(toState: completed,withID: task.objectID, incontext: context)
        })
        
        if task.hasNotification {
            if completed {
                NotificationScheduler.shared.rescheduleNotification(forTask: task)
            } else {
                NotificationScheduler.shared.deleteScheduledNotification(forTask: task)
            }
        }
    }
    
    func deleteTask(atIndexPath indexPath:IndexPath) {
        let task = self.objectFrom(publicIndexPath: indexPath)
        CotextOperationsHandler.performOnMain(action: { context in
            context.delete(task)
            NotificationScheduler.shared.deleteScheduledNotification(forTask: task)
        })
    }
    
    private func objectFrom(publicIndexPath indexPath:IndexPath) -> Task {
        let frc = indexPath.section == 1 ? self.completedTasksFRC : self.newTasksFRC
        return frc?.object(at: self.translateToLocal(indexPath: indexPath)) as! Task
    }
    
    private func translateToLocal(indexPath:IndexPath) -> IndexPath {
        return IndexPath.init(row: indexPath.row, section: 0)
    }
    
    private func translateToPublic(indexPath:IndexPath) -> IndexPath {
        return IndexPath.init(row: indexPath.row, section: 1)
    }    
    
    // MARK: - KVO
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(userSettings.sortType) {
            self.newTasksFRC = self.createTasksFetchedResultsController(completed: false)
            self.completedTasksFRC = self.createTasksFetchedResultsController(completed: true)
            self.delegate?.didResetData()
        }
    }
    
    
    // MARK: - FRC
    
    fileprivate func createTasksFetchedResultsController(completed:Bool) -> NSFetchedResultsController<NSFetchRequestResult> {
        
        let sortDescriptor = Task.taskListSortDescriptor()
        let predicate = Task.isCompletedPredicate(isCompleted: completed)
        
        return NSFetchedResultsControllerCreateMethods.createInMainContext(forEntity: Task.self,
                                                                           withSortDescriptors: [sortDescriptor],
                                                                           withPredicate: predicate,
                                                                           withDelegate: self)
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.dataWillChange()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        delegate?.dataDidChange()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        let publicIndexPath = indexPath == nil ? nil:
            (controller == completedTasksFRC ? self.translateToPublic(indexPath: indexPath!) : indexPath)
        let publicNewIndexPath = newIndexPath == nil ? nil:
            (controller == completedTasksFRC ? self.translateToPublic(indexPath: newIndexPath!) : newIndexPath)
        
        delegate?.didChange(anObject: anObject, at: publicIndexPath, for: type, newIndexPath: publicNewIndexPath)
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        delegate?.didChange(sectionWithIndex: sectionIndex, for: type)
    }
}

