# README #

This readme contains notes to the sourcecode and app architecture.

### Architecture ###
In this project I tried to use simple MVVM for data formatting and form validation. As I'm not very experienced with this pattern in iOS, I used it in two different ways:


1. In TaskListViewController I used it to absolutely remove Model information from View. It was necessary due to operations with Task object that would require use of ManagedObjectContext in ViewController.


2. In other viewcontrollers I used ViewModel mainly to validate data and manage temporary context. As a next step I'd like use viewmodel in CategoryListTableViewController in the same way as in TaskListViewcontroller and therefore remove duplicit tableview and FRC interaction code.

In the future I'd rather try flowcontroller to reuse similiar viewControllers, in this case for categories and create/edit forms.

### Task sections ###
I didn't use very pretty way to handle multiple sections, as FRC in relation to tableview hid empty sections which was causing crashes when creating/deleting new section and wrong section names. So I decided to handle it by multiple FRC and indexPath translator, which could be done more generically as it is now.


### Data preloading ###
I used the quickiest way for me to generate default data, but maybe better way would be using preloaded SQLite file. 


### Next 'features' would be: ###
I did not have as much free time as I wanted, so this may be my 'features' wishlist.

1.  Refactoring: Creating general TableViewController implementing ViewModelDataManagerDelegate and basic viewModel. Generic indexpath translation (or maybe I'll find better way to handle empty sections)

2. Localization - this would also remove unnecessary strings in storyboard.

3. Notification actions - maybe adding complete button to notification (that's why my target is iOS10, not 9, to use new notification API)